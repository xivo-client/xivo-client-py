# -*- coding: UTF-8 -*-

# Copyright (C) 2012  Avencall
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA..

import unittest
from cti.message_factory import MessageFactory
from cti.cti_login_ack import CtiLoginAck
from cti.cti_message import CtiMessage
from cti.cti_login_pass_ack import CtiLoginPassAck
from cti.cti_get_list import CtiGetList
from cti.cti_user_config import CtiUserConfig
from cti.cti_list import CtiList


class TestMessageFactory(unittest.TestCase):

    def setUp(self):
        self.message_factory = MessageFactory()

    def test_decode_login_id(self):
        msg_data = '{"sessionid": "wvrz4EClQS", "class": "login_id", "timenow": 1361358710.37, "xivoversion": "1.2"}'

        msg = self.message_factory.create(msg_data)

        self.assertTrue(isinstance(msg, CtiLoginAck))
        self.assertEqual(msg.sessionid, "wvrz4EClQS")

    def test_create_unknown_class(self):
        msg_data = '{"class": "mgfrt"}'

        msg = self.message_factory.create(msg_data)

        self.assertTrue(isinstance(msg, CtiMessage))

    def test_decode_login_pass_ack(self):
        msg_data = '{"capalist": [2], "replyid": 2, "class": "login_pass", "timenow": 1361375856.17}'

        msg = self.message_factory.create(msg_data)

        self.assertTrue(isinstance(msg, CtiLoginPassAck))
        self.assertEqual(msg.capalist[0], 2)

    def test_decode_getlist(self):
        msg_data = '{"class": "getlist","function": "listid", "listname": "users", "tipbxid": "xivo", "list": ["11", "12", "14", "17", "1", "3", "2", "4", "9"], "timenow": 1362735706.83}'
        msg = self.message_factory.create(msg_data)

        self.assertTrue(isinstance(msg, CtiList))
        self.assertEqual(msg.function, "listid")
        self.assertEqual(msg.listname, "users")
        self.assertEqual(msg.items, ["11", "12", "14", "17", "1", "3", "2", "4", "9"])

    def test_decode_updateconfig(self):
        msg_data = '''{"class": "getlist","function": "updateconfig", "listname": "users", "tipbxid": "xivo", "timenow": 1362748309.16, "tid": "14", 
                        "config": {"enablednd": 0, "destrna": "", "enablerna": 0, "firstname": "Boby", "profileclient": null, 
                        "lastname": "Dylan", "enableunc": 0, "destbusy": "", "enablebusy": 0, "voicemailid": null, "incallfilter": 0, 
                        "destunc": "", "enablevoicemail": 0, "enablexfer": 0, "fullname": "Boby Dylan", "agentid": 45, 
                        "enableclient": 0, "linelist": ["9"], "mobilephonenumber": ""}}'''

        msg = self.message_factory.create(msg_data)

        self.assertTrue(isinstance(msg, CtiUserConfig))
        self.assertEqual(msg.tid, "14")
        self.assertIsNotNone(msg.config)
        self.assertEqual(msg.listname, "users")
        self.assertEqual(msg.config.lastname, "Dylan")
        self.assertEqual(msg.config.firstname, "Boby")
        self.assertEqual(msg.config.agentid, "45")

    def test_decode_message_with_unknown_function(self):
        msg_data = '{"class": "getlist","function": "unknown"}'
        msg = self.message_factory.create(msg_data)

        self.assertTrue(isinstance(msg, CtiMessage))
