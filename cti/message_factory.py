# -*- coding: UTF-8 -*-

# Copyright (C) 2012  Avencall
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA..

from hashlib import sha1
from cti.cti_message_class import CtiMessageClass
from cti.cti_login_ack import CtiLoginAck
import json
from cti.cti_message import CtiMessage
from cti.cti_login_pass_ack import CtiLoginPassAck
from cti.cti_get_list import CtiGetList
from cti.cti_user_config import CtiUserConfig
from cti.cti_message_function import CtiMessageFunction
from cti.cti_list import CtiList
import itertools


class MessageFactory():

    messages = {
                CtiMessageClass.LoginId: CtiLoginAck,
                CtiMessageClass.LoginPass: CtiLoginPassAck,
                CtiMessageClass.GetList: CtiGetList
                }
    functions = {
                 CtiMessageFunction.update_config: CtiUserConfig,
                 CtiMessageFunction.list_id: CtiList
                 }

    def __init__(self):
        self.command_id = itertools.count(1)

    def create(self, message_data):
        message_json = json.loads(message_data)

        msg = CtiMessage()
        try:
            if message_json['class'] == CtiMessageClass.GetList:
                msg = self.create_class_instance(self.functions[message_json['function']], message_json)
            else:
                msg = self.create_class_instance(self.messages[message_json['class']], message_json)
        except KeyError:
            pass

        msg.payload = str(message_json)

        return msg

    def create_class_instance(self, class_type, data):
        instance = class_type()
        for k, v in data.items():
            if k == "config":
                self.create_config(instance.config, v)
            else:
                if k == "list":
                    k = "items"
                if k in dir(class_type):
                    setattr(instance, k, v)
        return instance

    def create_config(self, config, data):
        for k, v in data.items():
            if k in dir(config):
                setattr(config, k, str(v))

    def _new_login_id_msg(self, username):
        return {
            'class': 'login_id',
            'company': 'default',
            'ident': '-',
            'userlogin': username,
            'version': '9999',
            'xivoversion': '1.2',
            'commandid': self.command_id.next(),
        }

    def _new_login_pass_msg(self, password, sessionid):
        hashed_password = sha1(sessionid + ':' + password).hexdigest()
        return {
            'class': 'login_pass',
            'hashedpassword': hashed_password,
            'commandid': self.command_id.next(),
        }

    def _new_login_capas_msg(self, capaid):
        return {
            'class': 'login_capas',
            'capaid': capaid,
            'state': 'available',
            'lastconnwins': False,
            'loginkind': 'user',
            'commandid': self.command_id.next(),
        }

    def _new_record_message(self, channel, subcommand):
        return {
            'class': 'ipbxcommand',
            'command': 'record',
            'subcommand': subcommand,
            'channel': channel,
            'commandid': self.command_id.next(),
        }

    def _new_originate_message(self, userid, destination):
        return {
            'class': 'ipbxcommand',
            'command': 'originate',
            'source': ('user:xivo/%s' % userid),
            'destination': ('exten:xivo/%s' % destination),
            'commandid': self.command_id.next(),
        }

    def _new_log_agent_message(self, agent_phone):
        return {
            'class': 'ipbxcommand',
            'command': 'agentlogin',
            'agentphonenumber': agent_phone,
            'commandid': self.command_id.next(),
        }

    def _new_logout_agent_message(self):
        return {
            'class': 'ipbxcommand',
            'command': 'agentlogout',
            'commandid': self.command_id.next(),
        }

    def _new_pause_agent_message(self, agent_id):
        return {
            'class': 'ipbxcommand',
            'command': 'queuepause',
            'commandid': self.command_id.next(),
            'member': ('agent:xivo/%s' % agent_id),
            "queue": "queue:xivo/all"
        }

    def _new_get_users_message(self):
        return {
            'class': 'getlist',
            'function': 'listid',
            'listname': 'users',
            'tpbxid': 'xivo',
            'commandid': self.command_id.next()
        }

    def _new_get_user_message(self, user_id):
        return {
            'class': 'getlist',
            'function': 'updateconfig',
            'listname': 'users',
            'tid': user_id,
            'tpbxid': 'xivo',
            'commandid': self.command_id.next()
        }
