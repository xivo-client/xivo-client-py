# -*- coding: UTF-8 -*-

# Copyright (C) 2013  Avencall
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA..
#
# based on example from  Eli Bendersky (eliben@gmail.com)

import sys
import time
from PyQt4.QtCore import *
from PyQt4.QtGui import *


from gui.logwidget import LogWidget
from gui.cticlientview import CtiClientView
from ctilink import CTILink

SERVER_HOST = '192.168.56.101'
SERVER_PORT = 5003


class XivoClientWindow(QMainWindow):
    def __init__(self, reactor, parent=None):
        super(XivoClientWindow, self).__init__(parent)
        self.cticlientview = None
        self.reactor = reactor

        self.create_main_frame()
        self.createCtiLink()
        self.createIndicatorTimer()

    def create_main_frame(self):

        mainBox = QVBoxLayout()
        self.phoneBox = QHBoxLayout()
        mainBox.addLayout(self.phoneBox)
        self.log_widget = LogWidget()
        mainBox.addWidget(self.log_widget)

        main_frame = QWidget()
        main_frame.setLayout(mainBox)
        main_frame.setMinimumWidth(300)

        self.setCentralWidget(main_frame)

    def createCtiLink(self):
        self.cticlientview = CtiClientView(SERVER_HOST, self.onConnect)
        self.phoneBox.addLayout(self.cticlientview)

        self.cticlientview.ctiLink = CTILink(SERVER_HOST)
        self.cticlientview.ctiLink.setLogger(self.log)
        self.cticlientview.ctiLink.set_display(self.cticlientview)
        self.cticlientview.ctiLink.createClient()

    def createOneShotTimer(self, timerInSec, timerHandler):
        self.log('timer request sec : %s' % timerInSec)
        oneTimer = QTimer(self)
        oneTimer.setSingleShot(True)
        oneTimer.timeout.connect(timerHandler)
        oneTimer.start(timerInSec * 1000)

    def createIndicatorTimer(self):
        self.circle_timer = QTimer(self)
        self.circle_timer.timeout.connect(self.onIndicatorTimer)
        self.circle_timer.start(25)

    def onIndicatorTimer(self):
        self.cticlientview.connectIndicator.next()

    def onConnect(self, serverHost, networkClient):
        self.log("trying to connect to : %s on %s " % (serverHost, SERVER_PORT))
        self.connection = self.reactor.connectTCP(serverHost, SERVER_PORT, networkClient)

    def createTimer(self, intervalSecs, timerCallback):
        self.keepalive_timer = QTimer(self)
        self.keepalive_timer.timeout.connect(timerCallback)
        self.keepalive_timer.start(intervalSecs * 1000)

    def log(self, msg):
        timestamp = '[%010.3f]' % time.clock()
        self.log_widget.append(timestamp + ' ' + str(msg))

    def closeEvent(self, e):
        self.log("close event")
        self.reactor.stop()

#-------------------------------------------------------------------------------
if __name__ == "__main__":
    app = QApplication(sys.argv)
    app.setStyle("plastique")

    try:
        import qt4reactor
    except ImportError:
        # Maybe qt4reactor is placed inside twisted.internet in site-packages?
        from twisted.internet import qt4reactor
    qt4reactor.install()

    from twisted.internet import reactor
    mainwindow = XivoClientWindow(reactor)
    mainwindow.show()

    reactor.run()
