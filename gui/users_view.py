# -*- coding: UTF-8 -*-

# Copyright (C) 2012  Avencall
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA..

from PyQt4.QtGui import QTableView
from PyQt4.QtGui import QHeaderView


class UsersView(QTableView):

    def __init__(self):
        QTableView.__init__(self)
        vh = self.verticalHeader()
        vh.setVisible(False)

        hh = self.horizontalHeader()
        hh.setStretchLastSection(True)

        self.resizeColumnsToContents()

        self.setSortingEnabled(True)
        self.verticalHeader().setResizeMode(QHeaderView.ResizeToContents)

        self.setShowGrid(True)
        self.setFixedHeight(200)
