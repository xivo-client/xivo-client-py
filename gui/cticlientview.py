# -*- coding: UTF-8 -*-

# Copyright (C) 2012  Avencall
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA..

from PyQt4.QtCore import *
from PyQt4.QtGui import *
from gui.connectindicator import ConnectIndicator
from models.users_model import UsersModel
from gui.users_view import UsersView


class CtiClientView(QVBoxLayout):

    def __init__(self, serverHost, connectHandler):
        QVBoxLayout.__init__(self)
        self.serverHost = serverHost
        self.connectHandler = connectHandler
        self.tabledata = []
        self.createView()

    def createView(self):
        topGrid = QVBoxLayout()
        self.addLayout(topGrid)
        self.create_time_box(topGrid)
        self.create_user_login_box(topGrid)
        self.create_current_ids_box(topGrid)
        func_grid = QHBoxLayout()
        self.addLayout(func_grid)
        self.create_record_box(func_grid)
        self.create_originate_box(func_grid)
        self.create_agent_login_box(func_grid)
        self.create_get_config_box(func_grid)
        self.create_user_table(self)

    def create_time_box(self, top_layout):
        timeBox = QHBoxLayout()
        self.timeDateLabel = QLabel('..................')
        timeBox.addWidget(self.timeDateLabel)
        self.sessionid = QLabel('---------------')
        timeBox.addWidget(self.sessionid)
        self.connectIndicator = ConnectIndicator()
        timeBox.addWidget(self.connectIndicator)

        top_layout.addLayout(timeBox)

    def create_user_login_box(self, top_layout):
        loginbox = self._create_hbox(top_layout, 'User info')
        loginbox.addWidget(QLabel("user "))
        self.username = QLineEdit("marice")
        loginbox.addWidget(self.username)
        loginbox.addWidget(QLabel("pwd "))
        self.password = QLineEdit("sapr")
        loginbox.addWidget(self.password)
        self.connectButton = QPushButton('Connect !')
        self.connectButton.clicked.connect(self.onConnect)
        loginbox.addWidget(self.connectButton)

    def create_current_ids_box(self, top_layout):

        current_ids_box = self._create_hbox(top_layout, 'Current')
        current_ids_box.addWidget(QLabel('User id'))
        self.current_userid = QLineEdit('')
        current_ids_box.addWidget(self.current_userid)
        current_ids_box.addWidget(QLabel('agent id'))
        self.agent_id = QLineEdit('')
        current_ids_box.addWidget(self.agent_id)

    def create_record_box(self, top_layout):
        recordbox = self._create_vbox(top_layout, 'Record')
        recordbox.addWidget(QLabel('Channel'))
        self.channel = QLineEdit('SIP/')
        recordbox.addWidget(self.channel)
        self.start_record = QPushButton('Start Record')
        self.start_record.clicked.connect(self.on_start_record)
        recordbox.addWidget(self.start_record)
        self.stop_record = QPushButton('Stop Record')
        self.stop_record.clicked.connect(self.on_stop_record)
        recordbox.addWidget(self.stop_record)

    def create_originate_box(self, top_layout):
        originate_box = self._create_vbox(top_layout, 'Originate')
        originate_box.addWidget(QLabel('Destination'))
        self.destination = QLineEdit('')
        originate_box.addWidget(self.destination)
        self.originate = QPushButton('originate')
        self.originate.clicked.connect(self.on_originate)
        originate_box.addWidget(self.originate)

    def create_agent_login_box(self, top_layout):
        agent_login_box = self._create_vbox(top_layout, 'agent')
        agent_login_box.addWidget(QLabel('agent phone'))
        self.agent_phone = QLineEdit('')
        agent_login_box.addWidget(self.agent_phone)
        log_agent = QPushButton('login')
        log_agent.clicked.connect(self.on_log_agent)
        agent_login_box.addWidget(log_agent)
        pause_agent = QPushButton('pause')
        pause_agent.clicked.connect(self.on_pause_agent)
        agent_login_box.addWidget(pause_agent)
        logout_agent = QPushButton('logout')
        logout_agent.clicked.connect(self.on_logout_agent)
        agent_login_box.addWidget(logout_agent)

    def create_get_config_box(self, top_layout):
        config_box = self._create_vbox(top_layout, 'configuration')
        get_users = QPushButton('Get User')
        get_users.clicked.connect(self.on_get_user)
        config_box.addWidget(get_users)
        get_users = QPushButton('Get Users')
        config_box.addWidget(get_users)
        get_users.clicked.connect(self.on_get_users)

    def create_user_table(self, top_layout):
        user_box = self._create_vbox(top_layout, 'users')

        self.users_view = UsersView()

        header = ['id', 'firstname', 'lastname', 'agentid']
        self.user_model = UsersModel(self.tabledata, header, self)

        self.users_view.setModel(self.user_model)

        user_box.addWidget(self.users_view)
        self.users_view.clicked.connect(self.on_users_view)

        self.connect(self.user_model, SIGNAL("resiseColumns()"), self.model_changed)

    def model_changed(self):
        pass
        #self.users_view.resizeColumnsToContents()

    def _create_vbox(self, top_layout, group_name):
        box = QVBoxLayout()
        box.setAlignment(Qt.AlignTop)
        group = QGroupBox(group_name)
        group.setLayout(box)
        top_layout.addWidget(group)
        return box

    def _create_hbox(self, top_layout, group_name):
        box = QHBoxLayout()
        box.setAlignment(Qt.AlignTop)
        group = QGroupBox(group_name)
        group.setLayout(box)
        top_layout.addWidget(group)
        return box

    def get_user_info(self):
        return (str(self.username.text()), str(self.password.text()))

    def onConnect(self):
        self.connectHandler(self.ctiLink.host, self.ctiLink.clientlink)

    def show_sessionid(self, sessionid):
        self.sessionid.setText(sessionid)

    def on_start_record(self):
        self.ctiLink.start_record(str(self.channel.text()))

    def on_stop_record(self):
        self.ctiLink.stop_record(str(self.channel.text()))

    def on_originate(self):
        self.ctiLink.originate(str(self.current_userid.text()), str(self.destination.text()))

    def on_log_agent(self):
        self.ctiLink.log_agent(str(self.agent_phone.text()))

    def on_logout_agent(self):
        self.ctiLink.logout_agent()

    def on_pause_agent(self):
        self.ctiLink.pause_agent(str(self.agent_id.text()))

    def on_get_user(self):
        self.ctiLink.get_user(str(self.current_userid.text()))

    def on_get_users(self):
        self.ctiLink.get_users()

    def on_list_updated(self, listname, items):
        self.user_model.set_items(items)

    def on_users_view(self, index):
        user_id = str(index.model().data(index.sibling(index.row(), 0), Qt.DisplayRole).toString())
        self.current_userid.setText(user_id)
        agent_id = str(index.model().data(index.sibling(index.row(), 3), Qt.DisplayRole).toString())
        self.agent_id.setText(agent_id)

    def on_user_config_updated(self, user_id, config):
        self.user_model.update_item(user_id, config)
        self.current_userid.setText(user_id)
