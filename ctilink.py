# -*- coding: UTF-8 -*-

# Copyright (C) 2012  Avencall
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA..

from network.cticlientlinkfactory import CtiClientLinkFactory
import itertools
from cti.cti_login_ack import CtiLoginAck
from cti.cti_login_pass_ack import CtiLoginPassAck
from cti.cti_list import CtiList
from cti.cti_user_config import CtiUserConfig
from cti.message_factory import MessageFactory


class CTILink():

    message_factory = MessageFactory()

    def __init__(self, host):
        self.host = host
        self.sessionid = ""

    def setLogger(self, logger):
        self.log = logger

    def set_display(self, display):
        self.display = display

    def createClient(self):
        self.log('creating client link')
        self.clientlink = CtiClientLinkFactory(
                        self.on_connect_success,
                        self.on_connect_fail)
        self.clientlink.set_logger(self.log)
        self.clientlink.handle_unknown_message(self.on_unknown_class)
        self.clientlink.add_handler(CtiLoginAck, self.on_login_ack)
        self.clientlink.add_handler(CtiLoginPassAck, self.on_login_pass_ack)
        self.clientlink.add_handler(CtiList, self.on_get_list)
        self.clientlink.add_handler(CtiUserConfig, self.on_user_config)

    def on_connect_success(self):
        self.log('cti server connected')
        self.clientlink.send_msg(self.message_factory._new_login_id_msg(self.display.get_user_info()[0]))

    def on_connect_fail(self, reason):
        # reason is a twisted.python.failure.Failure  object
        self.log('Connection failed: %s' % reason.getErrorMessage())

    def on_unknown_class(self, message):
        self.log("message received  not processed: " + message.msgclass)

    def on_login_ack(self, login_ack):
        self.sessionid = login_ack.sessionid
        self.log('login ack received : sessionid : ' + self.sessionid)
        self.display.show_sessionid(login_ack.sessionid)
        self.clientlink.send_msg(self.message_factory._new_login_pass_msg(self.display.get_user_info()[1], self.sessionid))

    def on_login_pass_ack(self, login_pass_ack):
        self.capaid = login_pass_ack.capalist[0]
        self.log('login passwd ack received : capaid : ' + str(self.capaid))
        self.clientlink.send_msg(self.message_factory._new_login_capas_msg(self.capaid))

    def on_get_list(self, get_list):
        self.log('get list %s' % get_list.payload)
        self.display.on_list_updated(get_list.listname, get_list.items)

    def on_user_config(self, user_config):
        self.display.on_user_config_updated(user_config.tid, user_config.config)

    def start_record(self, channel):
        self.clientlink.send_msg(self.message_factory._new_record_message(channel, 'start'))

    def stop_record(self, channel):
        self.clientlink.send_msg(self.message_factory._new_record_message(channel, 'stop'))

    def originate(self, userid, destination):
        self.clientlink.send_msg(self.message_factory._new_originate_message(userid, destination))

    def log_agent(self, agent_phone):
        self.clientlink.send_msg(self.message_factory._new_log_agent_message(agent_phone))

    def logout_agent(self):
        self.clientlink.send_msg(self.message_factory._new_logout_agent_message())

    def pause_agent(self, agent_id):
        self.clientlink.send_msg(self.message_factory._new_pause_agent_message(agent_id))

    def get_users(self):
        self.clientlink.send_msg(self.message_factory._new_get_users_message())

    def get_user(self, user_id):
        self.clientlink.send_msg(self.message_factory._new_get_user_message(user_id))
