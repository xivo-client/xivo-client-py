# -*- coding: UTF-8 -*-

# Copyright (C) 2012  Avencall
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA..
from PyQt4.QtCore import *
from PyQt4.QtGui import *


class UsersModel(QAbstractTableModel):

    def __init__(self, datain, headerdata, parent=None, *args):
        QAbstractTableModel.__init__(self, parent, *args)
        self.arraydata = datain
        self.headerdata = headerdata

    def rowCount(self, parent):
        return len(self.arraydata)

    def columnCount(self, parent):
        if len(self.arraydata):
            return len(self.arraydata[0])
        else:
            return 0

    def data(self, index, role):
        if not index.isValid():
            return QVariant()
        elif role != Qt.DisplayRole:
            return QVariant()
        return QVariant(self.arraydata[index.row()][index.column()])

    def headerData(self, col, orientation, role):
        if orientation == Qt.Horizontal and role == Qt.DisplayRole:
            return QVariant(self.headerdata[col])
        return QVariant()

    def set_items(self, items):
        self.arraydata = []
        for item in items:
            data = []
            data.append(item)
            data.append('')
            data.append('')
            data.append('')
            self.arraydata.append(data)
        self.reset()
        self.emit(SIGNAL("resiseColumns()"))

    def update_item(self, user_id, config):
        user = []
        user.append(user_id)
        user.append(config.firstname)
        user.append(config.lastname)
        user.append(config.agentid)
        for index, item in enumerate(self.arraydata):
            if item[0] == user_id:
                self.arraydata[index] = user
        self.dataChanged.emit(self.createIndex(0, 0), self.createIndex(self.rowCount(0), self.columnCount(0)))
        self.emit(SIGNAL("resiseColumns()"))
