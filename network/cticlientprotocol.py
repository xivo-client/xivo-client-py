# -*- coding: UTF-8 -*-

# Copyright (C) 2012  Avencall
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA..

from twisted.protocols.basic import LineReceiver
import json
from cti.message_factory import MessageFactory


class CtiClientProtocol(LineReceiver):

    delimiter = '\n'

    message_factory = MessageFactory()

    def lineReceived(self, line):
        print "line received"
        print line
        message = self.message_factory.create(line)
        self.factory.handle_message(message)

    def connectionMade(self):
        self.factory.clientReady(self)

    def connectionLost(self, reason):
        print "connectionLost"
