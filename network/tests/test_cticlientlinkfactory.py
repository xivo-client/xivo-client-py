'''
Created on Jun 14, 2011

@author: lebleu1
'''
import unittest
from network.cticlientlinkfactory import CtiClientLinkFactory
from cti.cti_message_class import CtiMessageClass
from cti.cti_login_ack import CtiLoginAck
from cti.cti_message import CtiMessage
from mock import Mock


class TestCtiClientLinkFactory(unittest.TestCase):
    handled = False

    def setUp(self):
        self.handled = False
        self.unkown = False
        self.lastMessageClassReceived = ""
        self.clientFactory = CtiClientLinkFactory(self.unused, self.unused)
        self.clientFactory.set_logger(Mock())

    def unused(self):
        '''
        unused callback
        '''
    def handleUnknown(self, message):
        self.lastMessageClassReceived = message.msgclass
        self.unkown = True

    def handleMessage(self, message):
        self.lastMessageClassReceived = message.msgclass
        self.handled = True

    def testHandleMessage(self):
        self.clientFactory.add_handler(CtiLoginAck, self.handleMessage)
        self.clientFactory.handle_message(CtiLoginAck())
        self.assertTrue(self.handled, "message was not handled by callback")
        self.assertEquals(CtiMessageClass.LoginId, self.lastMessageClassReceived)

    def testHandleUnknownMessage(self):
        message = CtiMessage()
        self.clientFactory.handle_unknown_message(self.handleUnknown)
        self.clientFactory.handle_message(message)
        self.assertTrue(self.unkown)
        self.assertEquals(CtiMessageClass.Unknowned, self.lastMessageClassReceived)

    def testNoUnknownHandler(self):
        message = CtiMessage()
        try:
            self.clientFactory.handle_message(message)
        except KeyError:
            self.fail("should work even if unkown handler undefined")
