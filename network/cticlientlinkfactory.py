'''
Created on Jun 14, 2011

@author: lebleu1
'''
from twisted.internet.protocol import ClientFactory
from network.cticlientprotocol import CtiClientProtocol
import json


class CtiClientLinkFactory(ClientFactory):
    """ Created with callbacks for connection and receiving.
        send_msg can be used to send messages when connected.
    """

    protocol = CtiClientProtocol
    UNKNOWN_KEY = 'UNKNOWN'

    def __init__(
            self,
            connect_success_callback,
            connect_fail_callback):
        self.connect_success_callback = connect_success_callback
        self.connect_fail_callback = connect_fail_callback
        self.client = None
        self.messageHandlers = {}

    def set_logger(self, logger):
        self.logger = logger

    def clientConnectionFailed(self, connector, reason):
        self.connect_fail_callback(reason)

    def clientReady(self, client):
        self.client = client
        self.connect_success_callback()

    def send_msg(self, msg):
        if self.client:
            data = json.dumps(msg)
            self.logger(">>>> %s " % str(data))
            self.client.sendLine(data)

    def clientConnectionLost(self, connector, reason):
        self.logger('Lost connection.  Reason: %s' % reason)

    def onMessageReceived(self, data):
        print data

    def add_handler(self, msgclass, callback_handler):
        self.messageHandlers[msgclass] = callback_handler

    def handle_unknown_message(self, unknown_handler):
        self.messageHandlers[self.UNKNOWN_KEY] = unknown_handler

    def handle_message(self, message):
        self.logger('<<<< %s' % str(message.payload))
        if message.__class__ in self.messageHandlers:
            self.messageHandlers[message.__class__](message)
        else:
            if self.UNKNOWN_KEY in self.messageHandlers:
                self.messageHandlers[self.UNKNOWN_KEY](message)
            else:
                self.logger("ERROR no handler for message class <" + str(message.msgclass) + ">")
